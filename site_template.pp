Package {
  allow_virtual => true,
  }

include apache
apache::vhost {'test.com':}
include drush
include mysql
mysql::db {'test_db':
  user => 'test_user',
  password => 'test_password',
  }
drupal::settings {'test.com':
  db => 'test_db',
  user => 'test_user',
  password => 'test_password',
  }
include php
