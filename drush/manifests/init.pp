class drush {

   exec { "pear auto_discover" :
     command => "/usr/bin/pear config-set auto_discover 1",
     require => Package['php-pear'],
     before => Exec["pear update-channels"],
     creates => "/usr/bin/drush",
   }

   exec { "pear update-channels" :
     command => "/usr/bin/pear update-channels",
     require => Package['php-pear'],
     before => Exec["pear discover drush"],
     creates => "/usr/bin/drush",
   }
   exec { "pear discover drush" :
     command => "/usr/bin/pear channel-discover pear.drush.org",
     require => Package['php-pear'],
     before => Exec["pear install drush"],
     returns => [ 0, '', ' '],
     creates => "/usr/bin/drush",
   }
   exec { "pear install drush" :
    command => "/usr/bin/pear install drush/drush",
    require => Package['php-pear'],
    returns => [ 0, '', ' '],
    creates => "/usr/bin/drush",
   }

   file { "/etc/drush/":
     ensure  => directory,
     owner   => root,
     group   => root,
     mode    => 755,
   }

   file { "/etc/drush/drush.ini":
     ensure  => "present",
     source => "puppet:///modules/drush/drush.ini",
     owner   => root,
     group   => root,
     mode    => 644,
   }

}
