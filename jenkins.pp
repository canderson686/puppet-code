{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Calibri;}}
{\*\generator Msftedit 5.41.21.2510;}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang9\f0\fs22 #Jenkins Group\par
group \{ "jenkins":\par
ensure => present,\par
gid => 861\par
\}\par
\par
#Jenkins\par
user \{ "jenkins":\par
ensure  => present,\par
comment => "Jenkins Service Account",\par
home    => "/home/jenkins",\par
shell   => "/bin/bash",\par
uid     => "861",\par
gid     => "861",\par
password_max_age     => "2200",\par
require => Group["jenkins"]\par
\}\par
file \{ "/home/jenkins/":\par
ensure  => directory,\par
owner   => jenkins,\par
group   => jenkins,\par
mode    => 700,\par
require => [ User[jenkins], Group[jenkins] ]\par
\}\par
file \{ "/home/jenkins/.ssh/":\par
ensure  => directory,\par
owner   => jenkins,\par
group   => jenkins,\par
mode    => 700,\par
require => File["/home/jenkins/"]\par
\}\par
file \{ "/home/jenkins/.ssh/authorized_keys":\par
ensure  => present,\par
owner   => jenkins,\par
group   => jenkins,\par
mode    => 600,\par
require => File["/home/jenkins/.ssh/"]\par
\}\par
ssh_authorized_key\{ "jenkins_key\}":\par
ensure  => present,\par
key     => "AAAAB3NzaC1yc2EAAAABIwAAAQEA064XDtUOjQg9cDYHEALA5k025T7YIdM1Hm0+8UwgHZqrWERNdwntxa4eYwpcKbPO4P5ygucJctipNIlUcSpqqpM3fo0t7ikvL1Gnsyow6VwnZ05klfakWwsSN5T0870UOj4iiRLj4yknl/Tqab2u5N9NiR8Y52bofsZrqursoZGuamLwunRI3BswEEohELcwRQlqChTT+52eiMr838vJtjGvvVZuomLYptfve7JRrxcmaVXOH7IlozwVEAT4Uxi6HSB4zu8lhZ9z/QGGTWyax5pOXHPrHJ1M2VZBptCiaJyBnrrkMtWpiKh6K4z28gUefYmN5YUYe1UpJflUIZ2v6w==",\par
type    => ssh-rsa,\par
user    => jenkins,\par
require => File["/home/jenkins/.ssh/authorized_keys"],\par
\}\par
\par
augeas \{ "jenkins_sudo":\par
context => "/files/etc/sudoers",\par
changes => [\par
"set spec[user = '%jenkins']/user %jenkins",\par
"set spec[user = '%jenkins']/host_group/host ALL",\par
"set spec[user = '%jenkins']/host_group/command ALL",\par
"set spec[user = '%jenkins']/host_group/command/runas_user ALL",\par
"set spec[user = '%jenkins']/host_group/command/tag NOPASSWD",\par
"set Defaults[type=':jenkins']/type :jenkins",\par
"set Defaults[type=':jenkins']/requiretty/negate \\"\\"",\par
],\par
\}\par
}
 