class php {

    package { [ 'php', 'php-cli', 'php-pdo', 'php-mysql', 'php-xml', 'php-gd', 'php-mbstring', 'php-pear', 'git', 'php-pecl-apc', 'php-pecl-memcache' ] :
      ensure => 'present',
    }

    file { "/etc/php.ini":
      ensure  => "present",
      source => "puppet:///modules/php/php.ini",
      owner   => root,
      group   => root,
      mode    => 644,
      require => Package['php'],
      notify  => Service['httpd'],
      }

    file { "/etc/php.d/apc.ini":
      ensure  => "present",
      source => "puppet:///modules/php/apc.ini",
      owner   => root,
      group   => root,
      mode    => 644,
      require => Package['php'],
      notify  => Service['httpd'],
   }

}
