class mysql {

    package { [ 'mysql-server' ] :
      ensure => 'present',
    }

    file { "/etc/my.cnf":
      ensure  => "present",
      source => "puppet:///modules/mysql/my.cnf",
      owner   => root,
      group   => root,
      mode    => 644,
      notify => Service[mysqld],
      require => Package['mysql-server'],
    }

    service { 'mysqld':
      ensure => 'running',
      enable => true,
      hasrestart => true,
      hasstatus => true,
      subscribe => Package['mysql-server'],
      require => Package['mysql-server'],
    }

    file { '/var/log/mysql/':
      ensure => 'directory',
      owner   => mysql,
      group   => mysql,
      mode    => 644,
      require => Package['mysql-server'],
    }

    # Equivalent to /usr/bin/mysql_secure_installation without providing or setting a password
    exec { 'mysql_secure_installation':
      command => '/usr/bin/mysql -uroot -e "DELETE FROM mysql.user WHERE User=\'\'; DELETE FROM mysql.user WHERE User=\'root\' AND Host NOT IN (\'localhost\', \'127.0.0.1\', \'::1\'); DROP DATABASE IF EXISTS test; FLUSH PRIVILEGES;" mysql',
      creates => "/var/log/mysql/secure_run", 
      require => [ Package['mysql-server'], Service[mysqld]],
    }

    file { '/var/log/mysql/secure_run':
      ensure => 'present',
      owner   => mysql,
      group   => mysql,
      mode    => 644,
      require => Exec['mysql_secure_installation'],
    }

}
