define apache::vhost (
  $vhost_name = $name,
  )

{

	file { "/etc/httpd/conf.d/$vhost_name.conf":
	  ensure  => present,
          owner   => apache,
          group   => apache,
          mode    => 770,
          require => Package['httpd'],
	  content => template('apache/vhost.erb'),
	  notify  => Service['httpd'],
	}

        file { "/var/www/html/wwwroot/$vhost_name":
          ensure  => directory,
          owner   => apache,
          group   => apache,
          mode    => 770,
          require => Package['httpd'],
        }

        file { "/var/www/html/wwwroot/$vhost_name/docroot":
          ensure  => directory,
          owner   => apache,
          group   => apache,
          mode    => 770,
          require => File["/var/www/html/wwwroot/$vhost_name"],
        }

}
