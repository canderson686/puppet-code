class apache {

	package { [ "httpd", "mod_ssl" ] :
	  ensure => "present",
	}

	file { "/var/www/html/wwwroot/":
          ensure  => "directory",
          owner   => apache,
          group   => apache,
          mode    => 770,
          require => Package['httpd'],
        }

        file { "/etc/httpd/conf/httpd.conf":
          ensure  => "present",
          source => "puppet:///modules/apache/httpd.conf",
          owner   => root,
          group   => root,
          mode    => 644,
	  notify  => Service['httpd'],
          require => Package['httpd'],
        }

        file { "/var/www/sites-files":
          ensure  => directory,
          owner   => apache,
          group   => apache,
          mode    => 770,
          require => Package['httpd'],
        }

    service { 'httpd':
        ensure => 'running',
        enable => true,
        hasrestart => true,
        hasstatus => true,
        subscribe => Package['httpd'],
        require => File['/var/www/html/wwwroot/'],
    }

}
