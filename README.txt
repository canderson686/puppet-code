This is puppet code that will setup the prerequsties for a drupal site.  It includes the installation of the following:
- php
- apache
- drush
- mysql

It will also peform the configuration for:
- apache vhost
- creates the default database
- adds tunning for php, mysql, apache

To use the code, you will need to perform the following:
- install puppet
- install puppet master (optional)
  -  if using puppetmaster then you will need to perform all normall tasks to allow agent to communicate with the master
- create a site.pp file that includes the below between the "#":

############################################

include apache
apache::vhost {'test.com':}
include drush
include mysql
mysql::db {'test_db':
  user => 'test_user',
  password => 'test_password',
  }
include php

############################################

Notes:
Vhost - to change the name of the Vhost edit the value 'test.com' to the desired value
Vhost - to add an additional Vhost just copy and paste the line "apache::vhost {'test.com':" and ensure that you use a unique name
MySQL - to change the name of the Database, username, or passoword  edit the value to the desired value
MySQL - if you have a MySQL root password set (This is recomended), you will need to edit the value of "$mysql_password" in the file "puppet-code/mysql/manifests/db.pp"
