define drupal::settings (
  $site_name = $name, $db, $user, $password,
  )

{

	file { "/var/www/sites-files/${name}":
	  ensure  => directory,
          owner   => apache,
          group   => apache,
          mode    => 770,
          require => Package['httpd'],
	}

        file { "//var/www/sites-files/${name}/files":
          ensure  => directory,
          owner   => apache,
          group   => apache,
          mode    => 770,
          require => File["/var/www/sites-files/${name}"],
        }

        file { "/var/www/sites-files/${name}/tmp":
          ensure  => directory,
          owner   => apache,
          group   => apache,
          mode    => 770,
          require => File["/var/www/sites-files/${name}"],
        }

        file { "/var/www/html/wwwroot/${name}/docroot/sites/default/settings.local.inc":
          ensure  => file,
          owner   => apache,
          group   => apache,
          mode    => 770,
          require => Package['httpd'],
          content => template('drupal/settings.local.erb'),
        }

        file { "/var/www/html/wwwroot/${name}/docroot/sites/default/files":
          ensure  => link,
	  target => "/var/www/sites-files/${name}/files",
          owner   => apache,
          group   => apache,
          mode    => 770,
        }





}
